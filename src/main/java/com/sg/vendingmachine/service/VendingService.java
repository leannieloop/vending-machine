package com.sg.vendingmachine.service;

import com.sg.vendingmachine.exceptions.*;
import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Funds;
import com.sg.vendingmachine.dto.Item;

import java.util.Map;

public interface VendingService {
    Funds addFunds(String moneyInserted) throws InvalidAmountAddedException;
    Map<String, Item> getAvailableItems() throws FilePersistenceException;
    Item purchaseItem(String itemSelection, Funds funds)
            throws NoItemInventoryException, InsufficientFundsException, FilePersistenceException;
    boolean isChangeNeeded(Funds funds);
    Change returnChange(Funds funds) throws NoFundsRemainException;
}
