package com.sg.vendingmachine.service;

import com.sg.vendingmachine.dao.AuditDao;
import com.sg.vendingmachine.dao.MoneyDao;
import com.sg.vendingmachine.exceptions.*;
import com.sg.vendingmachine.dao.ItemDao;
import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Funds;
import com.sg.vendingmachine.dto.Item;

import java.math.BigDecimal;
import java.util.Map;
import java.util.stream.Collectors;

public class VendingServiceImpl implements VendingService {
    private ItemDao itemDao;
    private MoneyDao moneyDao;
    private AuditDao auditDao;

    public VendingServiceImpl(ItemDao itemDao, MoneyDao moneyDao, AuditDao auditDao) {
        this.itemDao = itemDao;
        this.moneyDao = moneyDao;
        this.auditDao = auditDao;
    }

    private Map<String, Item> itemsInStock;

    @Override
    public Funds addFunds(String moneyInserted) throws InvalidAmountAddedException {
        validateFunds(moneyInserted);
        return moneyDao.addFunds(moneyInserted);
    }

    @Override
    public Map<String, Item> getAvailableItems() throws FilePersistenceException {
        itemsInStock = itemDao.getAllItems()
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().getQuantity() > 0)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        return itemsInStock;
    }

    @Override
    public Item purchaseItem(String itemSelection, Funds funds)
            throws InsufficientFundsException, NoItemInventoryException, FilePersistenceException {
        if (isValidSelection(itemSelection) && areSufficientFunds(itemSelection, funds)) {
            Item item = itemDao.dispenseItem(itemSelection);
            moneyDao.subtractFunds(item.getPrice());
            return item;
        } else {
            return null;
        }
    }

    @Override
    public Change returnChange(Funds funds) throws NoFundsRemainException {
        try {
            if (funds.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                throw new NoFundsRemainException("ERROR: No change to return.");
            } else {
                return moneyDao.returnChange(funds);
            }
        } catch (NullPointerException e) {
            throw new NoFundsRemainException("ERROR: No change to return.");
        }
    }

    private void validateFunds(String moneyInserted) throws InvalidAmountAddedException {
        if (moneyInserted == null || "".equals(moneyInserted) || !isNumeric(moneyInserted)) {
            throw new InvalidAmountAddedException("ERROR: Please enter a valid dollar amount (e.g., 1.50).");
        }
    }

    private boolean isNumeric(String moneyInserted) {
        try {
            Double.parseDouble(moneyInserted);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    private boolean isValidSelection(String itemSelection) throws NoItemInventoryException {
        boolean isValid;
        if (!itemsInStock.containsKey(itemSelection)) {
            isValid = false;
            throw new NoItemInventoryException
                    ("ERROR: Invalid item selection. Item does not exist or is out of stock.");
        } else {
            isValid = true;
        }
        return isValid;
    }

    private boolean areSufficientFunds(String itemSelection, Funds funds) throws InsufficientFundsException {
        boolean isEnoughMoney;
        if (funds == null ||
                funds.getAmount().compareTo(itemsInStock.get(itemSelection).getPrice()) < 0) {
            isEnoughMoney = false;
            throw new InsufficientFundsException("ERROR: Insufficient funds.");
        } else {
            isEnoughMoney = true;
        }
        return isEnoughMoney;
    }

    public boolean isChangeNeeded(Funds funds) {
        try {
            return funds.getAmount().compareTo(BigDecimal.ZERO) > 0;
        } catch (NullPointerException e) {
            return false;
        }
    }
}
