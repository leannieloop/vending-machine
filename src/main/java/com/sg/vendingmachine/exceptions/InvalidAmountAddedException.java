package com.sg.vendingmachine.exceptions;

public class InvalidAmountAddedException extends Exception {

    public InvalidAmountAddedException(String message) {
        super(message);
    }
}
