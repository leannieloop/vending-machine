package com.sg.vendingmachine.exceptions;

public class NoFundsRemainException extends Exception {

    public NoFundsRemainException(String message) {
        super(message);
    }
}
