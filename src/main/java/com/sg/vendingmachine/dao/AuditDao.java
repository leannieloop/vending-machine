package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.exceptions.FilePersistenceException;

public interface AuditDao {
    void writeAuditEntry(String entry) throws FilePersistenceException;
}
