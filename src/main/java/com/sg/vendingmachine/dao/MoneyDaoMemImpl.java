package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Funds;

import java.math.BigDecimal;

public class MoneyDaoMemImpl implements MoneyDao {
    private Funds funds = new Funds("0");

    @Override
    public Funds addFunds(String moneyInserted) {
        funds.addAmount(moneyInserted);
        return funds;
    }

    @Override
    public Funds subtractFunds(BigDecimal itemPrice) {
        funds.setAmount(funds.getAmount().subtract(itemPrice));
        return funds;
    }

    @Override
    public Change returnChange(Funds funds) {
        Change change = new Change(funds.getAmount());
        funds.setAmount(BigDecimal.ZERO);
        return change;
    }
}
