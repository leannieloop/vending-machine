package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Item;
import com.sg.vendingmachine.exceptions.FilePersistenceException;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ItemDaoFileImpl implements ItemDao {
    private static final String INVENTORY_FILE = "itemInventory.txt";
    private static final String DELIMITER = ":;:";

    private Map<String, Item> items = new HashMap<>();

    @Override
    public Map<String, Item> getAllItems() throws FilePersistenceException {
        loadInventory();
        return items;
    }

    @Override
    public Item dispenseItem(String itemSelection) throws FilePersistenceException {
        Item item = items.get(itemSelection);
        item.setQuantity(item.getQuantity() - 1);
        writeInventory();
        return item;
    }

    private void loadInventory() throws FilePersistenceException {
        Scanner scanner;
        try {
            scanner = new Scanner(new BufferedReader(new FileReader(INVENTORY_FILE)));
        } catch (FileNotFoundException e) {
            throw new FilePersistenceException("ERROR: Could not load item inventory into memory.", e);
        }
        String currentLine;
        String[] itemAttributes;
        char keyChar = 'A';
        int keyInt = 1;
        while (scanner.hasNextLine()) {
            currentLine = scanner.nextLine();
            itemAttributes = currentLine.split(DELIMITER);
            Item item = new Item(itemAttributes[0], itemAttributes[1],itemAttributes[2]);
            items.put(Character.toString(keyChar) + keyInt, item);
            keyInt++;
            if (keyInt > 9) {
                keyInt = 1;
                keyChar++;
            }
        }
        scanner.close();
    }

    private void writeInventory() throws FilePersistenceException {
        PrintWriter out;
        try {
            out = new PrintWriter(new FileWriter(INVENTORY_FILE));
        } catch (IOException e) {
            throw new FilePersistenceException("Could not save item inventory.", e);
        }

        for (Item item : items.values()) {
            out.println(item.getName() + DELIMITER
                    + item.getPrice() + DELIMITER
                    + item.getQuantity());
            out.flush();
        }
        out.close();
    }
}
