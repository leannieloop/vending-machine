package com.sg.vendingmachine.dto;

import java.math.BigDecimal;

public class Change {

    private int quarters;
    private int dimes;
    private int nickels;
    private int pennies;

    public Change(BigDecimal funds) {
        this.pennies = funds.multiply(BigDecimal.valueOf(100)).intValue();
        this.quarters = pennies / 25;
        this.pennies = pennies % 25;
        this.dimes = pennies / 10;
        this.pennies = pennies % 10;
        this.nickels = pennies / 5;
        this.pennies = pennies % 5;
    }

    public int getQuarters() {
        return quarters;
    }

    public int getDimes() {
        return dimes;
    }

    public int getNickels() {
        return nickels;
    }

    public int getPennies() {
        return pennies;
    }

    @Override
    public String toString() {
        return " |Quarters: " + quarters + " |Dimes: " + dimes + " |Nickels: "
                + nickels + " |Pennies: " + pennies;
    }
}
