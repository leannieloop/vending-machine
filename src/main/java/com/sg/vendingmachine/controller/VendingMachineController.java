package com.sg.vendingmachine.controller;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Funds;
import com.sg.vendingmachine.dto.Item;
import com.sg.vendingmachine.exceptions.*;
import com.sg.vendingmachine.service.VendingService;
import com.sg.vendingmachine.ui.ConsoleView;

import java.util.HashMap;
import java.util.Map;

public class VendingMachineController {
    private ConsoleView view;
    private VendingService service;
    private Funds funds;

    public VendingMachineController(ConsoleView view, VendingService service) {
        this.view = view;
        this.service = service;
    }

    public void runVendingMachine() {
        displayAvailableItems();
        boolean run = true;
        while (run) {
            printMainMenu();
            int menuSelection = getMenuSelection();
            switch (menuSelection) {
                case 1:
                    displayAvailableItems();
                    break;
                case 2:
                    addFunds();
                    break;
                case 3:
                    purchaseItem();
                    if (view.shouldReturnChange()) {
                        returnChange();
                    }
                    break;
                case 4:
                    returnChange();
                    break;
                case 0:
                    returnChangeIfFundsRemain();
                    printExitMessage();
                    run = false;
                    break;
            }
        }
    }

    private void printMainMenu() {
        view.printMainMenu();
    }

    private int getMenuSelection() {
        boolean isValidSelection;
        int userMenuSelection = -1;
        do {
            try {
                userMenuSelection = view.getUserMenuSelection();
                isValidSelection = true;
            } catch (UnknownCommandException e) {
                isValidSelection = false;
                view.printErrorMessage(e.getMessage());
            }
        } while (!isValidSelection);
        return userMenuSelection;
    }

    private void displayAvailableItems() {
        Map<String, Item> itemsInStock = new HashMap<>();
        try {
            itemsInStock = service.getAvailableItems();
        } catch (FilePersistenceException e) {
            view.printErrorMessage(e.getMessage());
        }
        view.printAvailableItems(itemsInStock);
        view.userEnterToContinue();
    }

    private void addFunds() {
        view.printInsertMoneyBanner();
        boolean hasError;
        do {
            String moneyInserted = view.askUserToInsertMoney();
            try {
                funds = service.addFunds(moneyInserted);
                hasError = false;
            } catch (InvalidAmountAddedException e) {
                hasError = true;
                view.printErrorMessage(e.getMessage());
            }
        } while (hasError);
        view.printAvailableFunds(funds);
        view.userEnterToContinue();
    }

    private void purchaseItem() {
        String itemSelection = view.getUserItemSelection();
        try {
            Item item = service.purchaseItem(itemSelection, funds);
            view.printPurchaseSuccessMessage(item);
        } catch (NoItemInventoryException
                | InsufficientFundsException
                | FilePersistenceException e) {
            view.printErrorMessage(e.getMessage());
        }
    }

    private void returnChangeIfFundsRemain() {
        if (service.isChangeNeeded(funds)) {
            returnChange();
        }
    }

    private void returnChange() {
        view.printReturnChangeBanner();
        try {
            Change change = service.returnChange(funds);
            view.printChangeReturned(change);
        } catch (NoFundsRemainException e) {
            view.printErrorMessage(e.getMessage());
        }
        view.userEnterToContinue();
    }

    private void printExitMessage() {
        view.printExitMessage();
    }
}