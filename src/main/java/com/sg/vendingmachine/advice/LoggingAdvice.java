package com.sg.vendingmachine.advice;

import com.sg.vendingmachine.dao.AuditDao;
import com.sg.vendingmachine.exceptions.FilePersistenceException;
import org.aspectj.lang.JoinPoint;

public class LoggingAdvice {
    private AuditDao auditDao;

    public LoggingAdvice(AuditDao auditDao) {
        this.auditDao = auditDao;
    }

    public void createTransactionAuditEntry(JoinPoint jp, Object returnValue) {
        String auditEntry = jp.getSignature().getName();
        auditEntry += returnValue;
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (FilePersistenceException fpe) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }

    public void createExceptionAuditEntry(JoinPoint jp, Exception e) {
        Object[] args = jp.getArgs();
        String auditEntry = jp.getSignature().getName();
        for (Object arg : args) {
            auditEntry += " |Attempted " + arg + " |Exception " + e.getMessage() + " ";
        }
        try {
            auditDao.writeAuditEntry(auditEntry);
        } catch (FilePersistenceException fpe) {
            System.err.println("ERROR: Could not create audit entry in LoggingAdvice.");
        }
    }
}
