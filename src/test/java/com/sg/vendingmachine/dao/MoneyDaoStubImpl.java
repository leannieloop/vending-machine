package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Funds;

import java.math.BigDecimal;

public class MoneyDaoStubImpl implements MoneyDao {
    @Override
    public Funds addFunds(String moneyInserted) {
        return null;
    }

    @Override
    public Funds subtractFunds(BigDecimal itemPrice) {
        return null;
    }

    @Override
    public Change returnChange(Funds funds) {
        return null;
    }
}
