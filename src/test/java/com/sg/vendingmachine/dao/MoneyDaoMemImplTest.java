package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Change;
import com.sg.vendingmachine.dto.Funds;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class MoneyDaoMemImplTest {
    private MoneyDaoMemImpl dao = new MoneyDaoMemImpl();

    @Test
    public void testAddSevenDollars() {
        assertEquals(new Funds("7").getAmount(), dao.addFunds("7").getAmount());
    }

    @Test
    public void testSubtractSevenDollars() {
        assertEquals(new Funds("-7").getAmount(), dao.subtractFunds(new BigDecimal("7")).getAmount());
    }

    @Test
    public void testReturnChangeFortyFourCents() {
        Change change = dao.returnChange(new Funds("0.44"));
        assertEquals(1, change.getQuarters());
        assertEquals(1, change.getDimes());
        assertEquals(1, change.getNickels());
        assertEquals(4, change.getPennies());
    }
}