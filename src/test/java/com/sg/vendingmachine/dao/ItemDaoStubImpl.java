package com.sg.vendingmachine.dao;

import com.sg.vendingmachine.dto.Item;
import com.sg.vendingmachine.exceptions.FilePersistenceException;

import java.util.HashMap;
import java.util.Map;

public class ItemDaoStubImpl implements ItemDao {

    private Map<String, Item> testItems = new HashMap<>();

    public ItemDaoStubImpl() {
        Item availableItem = new Item("Chips","1.00", "1");
        testItems.put("T1", availableItem);

        Item unavailableItem = new Item("Candy", "1.00", "0");
        testItems.put("T0", unavailableItem);
    }

    @Override
    public Map<String, Item> getAllItems() throws FilePersistenceException {
        return testItems;
    }

    @Override
    public Item dispenseItem(String itemSelection) throws FilePersistenceException {
        return testItems.get(itemSelection);
    }
}
